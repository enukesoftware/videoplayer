package com.enuke.myapplication;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;
import java.util.TimeZone;

public class MainActivity extends AppCompatActivity {

    private Button buttonPlayUrlVideo;
    private Button buttonPlayDefaultVideo;
    private VideoData videoData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonPlayUrlVideo = findViewById(R.id.buttonPlayUrlVideo);
        buttonPlayDefaultVideo = findViewById(R.id.buttonPlayDefaultVideo);

        buttonPlayUrlVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = ExoPlayerActivity.getStartIntent(MainActivity.this, VideoPlayerConfig.DEFAULT_VIDEO_URL,videoData);
                startActivity(mIntent);
            }
        });

        buttonPlayDefaultVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickVideo();
            }
        });
    }

    /**
     * pick video from gallery
     *
     */
    private void pickVideo() {
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("video/mp4");
        startActivityForResult(intent, 101);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            Uri selectedMediaUri = Objects.requireNonNull(data).getData();
            switch (requestCode) {
                case 101:
                    videoData = getFileDetails(selectedMediaUri);
                    if (Double.parseDouble(videoData.getMbSize()) < 50.00) {
//                        imgVideoThumb.setImageBitmap(videoData.getThumbnail());
//                        tvVideoName.setText(videoData.getFileName());
//                        tvVideoSize.setText(videoData.getMbSize() + "mb");
//                        tvVideoTime.setText(videoData.getVideoTime());
                        Toast.makeText(this, "Picked "+videoData.getFileName(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(this, "Large Size", Toast.LENGTH_SHORT).show();
                    }
                    break;

            }


        }
    }

    /**
     * acccess file details from uri
     *
     * @param returnUri
     * @return
     */
    private VideoData getFileDetails(Uri returnUri) {

        String filePath = Utils.getFilePath(this, returnUri);
        DecimalFormat df = new DecimalFormat("##0.00");
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        retriever.setDataSource(this, returnUri);
        String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        long timeInMillisec = Long.parseLong(time);
        Date date = new Date(timeInMillisec);
        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
        formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        String videoTime = formatter.format(date);
        String[] separated = videoTime.split(":");
        try {
            videoTime = separated[0] + "h:" + separated[1] + "m:" + separated[2] + "s";
        } catch (Exception e) {

        }
        retriever.release();

        Cursor returnCursor = getContentResolver().query(returnUri, null, null, null, null);
        int nameIndex = Objects.requireNonNull(returnCursor).getColumnIndex(OpenableColumns.DISPLAY_NAME);
        int sizeIndex = returnCursor.getColumnIndex(OpenableColumns.SIZE);

        returnCursor.moveToFirst();

        String fileName = returnCursor.getString(nameIndex);
        String fullPath = filePath;
        double byteSize = (double) returnCursor.getLong(sizeIndex);
        String mbSize = df.format(byteSize / 1000000);
        int kbSize = (int) (byteSize / 1000);
        Bitmap thumbnail = ThumbnailUtils.createVideoThumbnail(fullPath, MediaStore.Images.Thumbnails.MICRO_KIND);
        returnCursor.close();

        return new VideoData(returnUri, fileName, fullPath, String.valueOf(byteSize), mbSize, String.valueOf(kbSize), thumbnail, videoTime);

    }
}
