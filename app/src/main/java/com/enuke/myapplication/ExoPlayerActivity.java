package com.enuke.myapplication;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultAllocator;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class ExoPlayerActivity extends AppCompatActivity implements Player.EventListener {

    private final int MINUTES = 60;
    private final int SECONDS = 60;
    private final int MILLI_SECONDS = 1000;

    private static final String TAG = "ExoPlayerActivity";

    private static final String KEY_VIDEO_URI = "video_uri";

    PlayerView videoFullScreenPlayer;
    ProgressBar spinnerVideoDetails;
    ImageView imageViewExit;
    TextView tvRemainingTime;
    TextView tvCurrentPositionTime;
    ImageView imgLandscape;
    ImageView imgPortrait;
    ImageView imgVolumeUp;
    ImageView imgVolumeOff;

    String videoUri;
    SimpleExoPlayer player;
    Handler mHandler = new Handler();
    float currentVolume;
    private VideoData videoData;

    Runnable mRunnable = new Runnable() {
        @Override
        public void run() {

            if(player !=null){
//                int time = (int) ((player.getCurrentPosition()*100)/player.getDuration());
//                Log.e("SEEKBAR:",player.getCurrentPosition()+"");
//                Log.e("CURRENT_POSITION:",(((player.getCurrentPosition()/500)+1)/2)+"");
//                Log.e("CURRENT_MILLI:",player.getCurrentPosition()+"");
//                Log.e("DURATION:",player.getDuration()+"");
//                tvRemainingTime.setText("- "+((player.getCurrentPosition()/500)+1)/2);
                tvCurrentPositionTime.setText(getCurrentPositionTime(player.getCurrentPosition(),player.getDuration()));
                tvRemainingTime.setText(getRemainingTime(player.getCurrentPosition(),player.getDuration()));

            }

            mHandler.postDelayed(this,1000);
        }
    };

    public static Intent getStartIntent(Context context, String videoUri,VideoData videoData) {
        Intent intent = new Intent(context, ExoPlayerActivity.class);
        intent.putExtra(KEY_VIDEO_URI, videoUri);
        intent.putExtra("videoData",videoData);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exo_player);

        videoFullScreenPlayer = findViewById(R.id.videoFullScreenPlayer);
        spinnerVideoDetails = findViewById(R.id.spinnerVideoDetails);
        imageViewExit = findViewById(R.id.imgExit);
        tvRemainingTime = videoFullScreenPlayer.findViewById(R.id.exo_remaining);
        tvCurrentPositionTime = videoFullScreenPlayer.findViewById(R.id.exo_current_position);
        imgLandscape = findViewById(R.id.imgLandscape);
        imgPortrait = findViewById(R.id.imgPortrait);
        imgVolumeUp = findViewById(R.id.imgVolumeUp);
        imgVolumeOff = findViewById(R.id.imgVolumeOff);

        imgVolumeOff.setVisibility(View.GONE);
        imgVolumeUp.setVisibility(View.VISIBLE);
        imgPortrait.setVisibility(View.GONE);
        imgLandscape.setVisibility(View.VISIBLE);

        imgVolumeUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentVolume = player.getVolume();
                player.setVolume(0f);
                imgVolumeUp.setVisibility(View.GONE);
                imgVolumeOff.setVisibility(View.VISIBLE);
            }
        });

        imgVolumeOff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                player.setVolume(currentVolume);
                imgVolumeUp.setVisibility(View.VISIBLE);
                imgVolumeOff.setVisibility(View.GONE);
            }
        });

        imgLandscape.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
                imgLandscape.setVisibility(View.GONE);
                imgPortrait.setVisibility(View.VISIBLE);

            }
        });

        imgPortrait.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
                imgPortrait.setVisibility(View.GONE);
                imgLandscape.setVisibility(View.VISIBLE);
            }
        });

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        getSupportActionBar().hide();

        if (getIntent().hasExtra(KEY_VIDEO_URI)) {
            videoUri = getIntent().getStringExtra(KEY_VIDEO_URI);
        }

        if(getIntent().hasExtra("videoData")){
            videoData = (VideoData) getIntent().getParcelableExtra("videoData");
        }

        imageViewExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        setUp();
    }

    private void setUp() {
        initializePlayer();
//        if (videoUri == null) {
//            return;
//        }

        if(videoData == null){
            return;
        }
        buildMediaSource(videoData.getVideoUri());
    }

    private void initializePlayer() {
        if (player == null) {
            // 1. Create a default TrackSelector
            LoadControl loadControl = new DefaultLoadControl(
                    new DefaultAllocator(true, 16),
                    VideoPlayerConfig.MIN_BUFFER_DURATION,
                    VideoPlayerConfig.MAX_BUFFER_DURATION,
                    VideoPlayerConfig.MIN_PLAYBACK_START_BUFFER,
                    VideoPlayerConfig.MIN_PLAYBACK_RESUME_BUFFER, -1, true);

            BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
            TrackSelection.Factory videoTrackSelectionFactory =
                    new AdaptiveTrackSelection.Factory(bandwidthMeter);
            TrackSelector trackSelector =
                    new DefaultTrackSelector(videoTrackSelectionFactory);
            // 2. Create the player
            player = ExoPlayerFactory.newSimpleInstance(new DefaultRenderersFactory(this), trackSelector, loadControl);
            videoFullScreenPlayer.setPlayer(player);
            tvCurrentPositionTime.setText(getCurrentPositionTime(0L,0L));
            tvRemainingTime.setText(getRemainingTime(0L,0L));
        }


    }

    private void buildMediaSource(Uri mUri) {
        // Measures bandwidth during playback. Can be null if not required.
        DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        // Produces DataSource instances through which media data is loaded.
        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(this,
                Util.getUserAgent(this, getString(R.string.app_name)), bandwidthMeter);
        // This is the MediaSource representing the media to be played.
        MediaSource videoSource = new ExtractorMediaSource.Factory(dataSourceFactory)
                .createMediaSource(mUri);
        // Prepare the player with the source.
        player.prepare(videoSource);
        player.setPlayWhenReady(true);
        player.addListener(this);
//        mHandler.post(mRunnable);
    }

    private void releasePlayer() {
        if (player != null) {
            player.release();
            player = null;
        }
    }

    private void pausePlayer() {
        if (player != null) {
            player.setPlayWhenReady(false);
            player.getPlaybackState();
        }
    }

    private void resumePlayer() {
        if (player != null) {
            player.setPlayWhenReady(true);
            player.getPlaybackState();

        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        pausePlayer();
        if (mRunnable != null) {
            mHandler.removeCallbacks(mRunnable);
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        resumePlayer();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        releasePlayer();
    }

    @Override
    public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {
    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

    }

    @Override
    public void onLoadingChanged(boolean isLoading) {

    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
        switch (playbackState) {

            case Player.STATE_BUFFERING:
                spinnerVideoDetails.setVisibility(View.VISIBLE);
                break;
            case Player.STATE_ENDED:
                // Activate the force enable
                break;
            case Player.STATE_IDLE:

                break;
            case Player.STATE_READY:
                spinnerVideoDetails.setVisibility(View.GONE);
                mHandler.removeCallbacks(mRunnable);
                mHandler.post(mRunnable);
                break;
            default:
                // status = PlaybackStatus.IDLE;
                break;
        }
    }

    @Override
    public void onRepeatModeChanged(int repeatMode) {

    }

    @Override
    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {

    }

    @Override
    public void onPositionDiscontinuity(int reason) {

    }

    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

    }

    @Override
    public void onSeekProcessed() {

    }

    private String getCurrentPositionTime(Long currentTime,Long duration){
        if(currentTime <= 0){
            if(duration<(MINUTES * SECONDS * MILLI_SECONDS)){
                return "00:00";
            }else {
                return "00:00:00";
            }

        }else {
            Date date = new Date(currentTime);
            SimpleDateFormat formatter;
            if(duration<(MINUTES * SECONDS * MILLI_SECONDS)){
                formatter = new SimpleDateFormat("mm:ss");
            }else {
                formatter = new SimpleDateFormat("HH:mm:ss");
            }

            formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
            return formatter.format(date);
        }

    }

    private String getRemainingTime(Long currentTime,Long duration){
        Long remainginTime = duration - currentTime;
        if(remainginTime <= 0){
            if(duration<(MINUTES * SECONDS * MILLI_SECONDS)){
                return "- 00:00";
            }else {
                return "- 00:00:00";
            }

        }else {
            Date date = new Date(remainginTime);
            SimpleDateFormat formatter;
            if(duration<(MINUTES * SECONDS * MILLI_SECONDS)){
                formatter = new SimpleDateFormat("mm:ss");
            }else {
                formatter = new SimpleDateFormat("HH:mm:ss");
            }

            formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
            return ("- " + formatter.format(date));
        }

    }

}
